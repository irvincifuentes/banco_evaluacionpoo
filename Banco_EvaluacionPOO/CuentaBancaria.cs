﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banco_EvaluacionPOO
{
    public abstract class CuentaBancaria 
    {
        public string NumerodeCuenta { get; set; }
        public string PropietarioCuenta { get; set; }


        public CuentaBancaria()
        {

        }

        public abstract string imprimir_Datos(); // Método abstracto, aplicación de polimorfismo

    }
}
