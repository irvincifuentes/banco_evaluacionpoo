﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banco_EvaluacionPOO
{
    class Poliza : CuentaBancaria // Herencia, debe heredar el método abstract para imprimir los datos
    {
        public float ValorPoliza { get; set; }
        public int MesesPoliza { get; set; }
        public float PorcentajeInteres { get; set; }


        public float CalculoInteresPoliza()
        {
            float InteresGanadoPoliza;

            InteresGanadoPoliza = this.ValorPoliza + this.PorcentajeInteres * this.MesesPoliza;

            return InteresGanadoPoliza;
        }

        public override string imprimir_Datos()
        {
             return "Los datos de la cuenta son los siguientes:" + " \nPropietario de la cuenta: " + this.PropietarioCuenta + "\nNumero de cuenta: " + this.NumerodeCuenta
                + "\nValor Póliza: " + this.ValorPoliza + "\nInterés ganado: " + CalculoInteresPoliza();
        }
    }
}
