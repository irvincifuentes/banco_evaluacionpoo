﻿using System;
using System.Collections.Generic;


namespace Banco_EvaluacionPOO
{
    class Program
    {



        static void Main(string[] args)
        {

            List<CuentaBancaria> AniadirObjeto = new List<CuentaBancaria>();
            CuentaAhorros cuentaAhorros = new CuentaAhorros();
            CuentaCorriente cuentaCorriente = new CuentaCorriente();
            Poliza poliza = new Poliza();

            cuentaAhorros.PropietarioCuenta = "Irvin Cifuentes";
            cuentaAhorros.SaldoCuenta = 200;
            cuentaAhorros.NumerodeCuenta = "054848444";
            cuentaAhorros.PorcentajeInteres = 1; //Valor variable

            cuentaCorriente.PropietarioCuenta = "Marco Perez";
            cuentaCorriente.SaldoCuenta = 300;
            cuentaCorriente.NumerodeCuenta = "625265";

            poliza.PropietarioCuenta = "Javier Hernandez";
            poliza.NumerodeCuenta = "626565";
            poliza.MesesPoliza = 11;
            poliza.PorcentajeInteres = 2;
            poliza.ValorPoliza = 100;

            AniadirObjeto.Add(cuentaAhorros);
            AniadirObjeto.Add(cuentaCorriente);
            AniadirObjeto.Add(poliza);

            foreach (CuentaBancaria item in AniadirObjeto)
            {
                Console.WriteLine(item.imprimir_Datos());
                Console.WriteLine("---------------------------------------");
            }



        }


    }
}
