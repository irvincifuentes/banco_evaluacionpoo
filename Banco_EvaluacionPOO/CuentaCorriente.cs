﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banco_EvaluacionPOO
{
    public class CuentaCorriente : CuentaBancaria // Herencia
    {
        public float SaldoCuenta { get; set; }
        public float PorcentajeInteres { get; set; }
        public float PorcentajeMantenimiento { get; set; }

        public CuentaCorriente()
        {

        }

        public float CalculoInteresCorriente() 
        {
            float InteresGanadoCorriente;

            this.PorcentajeMantenimiento = 1 / 100 * this.SaldoCuenta;
            this.PorcentajeInteres = 5 / 100 * this.SaldoCuenta;

            InteresGanadoCorriente = this.SaldoCuenta + PorcentajeInteres - PorcentajeMantenimiento;

            return InteresGanadoCorriente;

        }

        public override string imprimir_Datos()
        {
            return "Los datos de la cuenta son los siguientes:" + " \nPropietario de la cuenta: " + this.PropietarioCuenta + "\nNumero de cuenta: " + this.NumerodeCuenta
               + "\nSaldo de cuenta: " + this.SaldoCuenta + "\nInterés ganado: " + CalculoInteresCorriente();
        }
    }
}
