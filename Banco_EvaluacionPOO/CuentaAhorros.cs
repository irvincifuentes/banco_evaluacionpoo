﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banco_EvaluacionPOO
{
    public class CuentaAhorros : CuentaBancaria // Herencia
    {
        public float SaldoCuenta { get; set; }
        public float PorcentajeInteres { get; set; }

        public CuentaAhorros()
        {

        }

        
        public float CalculoInteresAhorros()
        {

            float InteresGanadoAhorros;

            InteresGanadoAhorros= this.SaldoCuenta + this.PorcentajeInteres;

            return InteresGanadoAhorros;
        }

        public override string imprimir_Datos() // Método abstracto, aplicación de polimorfismo
        {
            return "Los datos de la cuenta son los siguientes:" + " \nPropietario de la cuenta: " + this.PropietarioCuenta + "\nNumero de cuenta: " + this.NumerodeCuenta
                + "\nSaldo de cuenta: " + this.SaldoCuenta + "\nInterés ganado: " + CalculoInteresAhorros();
        }
    }
}
